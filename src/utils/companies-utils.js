import moment from "moment";

const formatTimeSlotData = (values, companyId) => {
  return values.reduce((tempArr, { start_time, end_time }) => {
    const itemsForThisCompanyTimeSlots = [];
    const weekDay = moment(start_time).format("dddd");
    const slot = `${moment(start_time).format("LT")} - ${moment(
      end_time
    ).format("LT")}`;
    const slotId = `${start_time}-${end_time}`;

    itemsForThisCompanyTimeSlots.push({
      companyId: `${companyId}-${slotId}`,
      fullDate: moment(start_time).format("MMM Do YY"),
      weekDay,
      slot: slot,
    });
    return [...tempArr, ...itemsForThisCompanyTimeSlots];
  }, []);
};

export const formatCompanyData = (data) => {
  return data.reduce((tempArr, { id, name, type, time_slots }) => {
    const itemsForThisCompany = [];
    const companyId = id;
    const slots = formatTimeSlotData(time_slots, companyId);
    const groupByDay = slots.reduce((group, slot) => {
      const { weekDay } = slot;
      group[weekDay] = group[weekDay] ?? [];
      group[weekDay].push(slot);
      return group;
    }, {});

    itemsForThisCompany.push({
      name,
      type,
      reservation: [],
      availableTimeSlots: groupByDay,
    });

    return [...tempArr, ...itemsForThisCompany];
  }, []);
};

export const groupByDay = (timeSlots) => {
  timeSlots.reduce((group, slot) => {
    const { weekDay } = slot;
    group[weekDay] = group[weekDay] ?? [];
    group[weekDay].push(slot);
    return group;
  }, {});
};

export const groupByDateAndTime = (timeSlots) => {
  //   timeSlots.reduce((group, slot) => {
  //     const { weekDay } = slot;
  //     group[weekDay] = group[weekDay] ?? [];
  //     group[weekDay].push(slot);
  //     return group;
  //   }, {});
};
