import React, { useEffect, useState, useMemo } from "react";
import { formatCompanyData } from "../utils/companies-utils";

const Grid = () => {
  const [companies, setCompanies] = useState([]);
  const [selectedSlot, setSelectedSlot] = useState([]);
  const [sortedArr, setSortedArr] = useState([]);
  const [error, setError] = useState(null);

  const removeDuplicate = (slotId) => {
    let uniqueArray = [...selectedSlot];
    let filtered = selectedSlot.filter((item) => item !== slotId);

    uniqueArray = Array.from(new Set(filtered));

    return uniqueArray;
  };

  const changeState = (newArr) => {
    setSortedArr(newArr);
  };

  const slotButtonButtonHandler = (slotId) => {
    if (selectedSlot.includes(slotId)) {
      setSelectedSlot(selectedSlot.filter((item) => item !== slotId));
      return true;
    }
    selectedSlot.forEach((slot) => {
      let newArr = [...selectedSlot];
      let id = slotId[0];

      if (slot.startsWith(id)) {
        if (selectedSlot.includes(slot)) {
          let index = selectedSlot.indexOf(slot);
          if (index !== -1) {
            selectedSlot[index] = slotId;
          }
          newArr = removeDuplicate(slot);
        }
        return changeState(newArr);
      } else {
        return setSelectedSlot(selectedSlot.concat(slotId));
      }
    });

    return setSelectedSlot(selectedSlot.concat(slotId));
  };

  useEffect(() => setSelectedSlot(sortedArr), [sortedArr]);

  useEffect(() => {
    fetch("http://localhost:3000/data")
      .then((response) => response.json())
      .then((data) => {
        if (data?.length) {
          const formattedTimeSlots = formatCompanyData(data);
          setCompanies(formattedTimeSlots);
        }
      })
      .catch((e) => {
        setError(e.toString());
        setError(e);
        console.log("There was an error!", error);
      });
  }, [error]);

  const companyTimeSlots = companies.map((company) => {
    return (
      <div
        key={company.companyId}
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          margin: "0 30px",
        }}
      >
        <h2>{company.name}</h2>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            backgroundColor: "lightblue",
            width: "100%",
            minHeight: "120px",
          }}
        >
          <h3>Booked:</h3>
          {Object.keys(company.availableTimeSlots).map((key) => {
            return (
              <div key={key}>
                {company.availableTimeSlots[key].map((slot) => {
                  let val;
                  if (selectedSlot.includes(slot.companyId)) {
                    return (val = (
                      <button
                        key={slot.companyId}
                        value={slot.companyId}
                        onClick={(e) => slotButtonButtonHandler(e.target.value)}
                      >
                        {slot.slot} - {slot.fullDate}
                      </button>
                    ));
                  }
                  return val;
                })}
              </div>
            );
          })}
        </div>
        <div
          style={{
            textJustify: "center",
            overflowY: "scroll",
            padding: "0 20px",
          }}
        >
          {Object.keys(company.availableTimeSlots).map((key) => {
            return (
              <div
                style={{
                  margin: "0 30px",
                }}
                key={key}
              >
                <h3
                  style={{
                    textAlign: "center",
                  }}
                >
                  {key}
                </h3>
                <ul className="listblock">
                  {company.availableTimeSlots[key].map((slot) => {
                    return (
                      <li key={slot.companyId}>
                        <button
                          className={
                            selectedSlot.includes(slot.companyId)
                              ? "disabledBtn"
                              : null
                          }
                          // disabled={selectedSlot.includes(slot.companyId)}
                          value={slot.companyId}
                          onClick={(e) =>
                            slotButtonButtonHandler(e.target.value)
                          }
                        >
                          {slot.slot} - {slot.fullDate}
                        </button>
                      </li>
                    );
                  })}
                </ul>
              </div>
            );
          })}
        </div>
      </div>
    );
  });

  return (
    <div
      className="container"
      style={{
        display: "flex",
        padding: "0 50px",
        justifyContent: "space-between",
        height: "100vh",
      }}
    >
      {companyTimeSlots}
    </div>
  );
};

export default Grid;
